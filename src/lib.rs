use std::collections::HashMap;
use cgmath::num_traits::FromPrimitive;
use ordered_float::OrderedFloat;
use winit::event::{ElementState, KeyboardInput, VirtualKeyCode, ScanCode};
use serde::{Serialize, Deserialize};

type Vector2 = cgmath::Vector2<f32>;

#[derive(Serialize, Deserialize, Eq, PartialEq, Hash, Clone, Debug)]
pub enum ActionKey {
	/// A key represented by a scancode, useful for inputs that depend more on key location than syntactic meaning.
	ScancodeId(u32),
	/// A key represented by it's virtual meaning, useful for inputs that depend more on a key's syntactic meaning
	/// than physical location.
	VirtualId(VirtualKeyCode),
}

/// An identifier to register a certain key or key combo for an action.
#[derive(Serialize, Deserialize, Eq, PartialEq, Hash, Clone, Debug)]
pub enum Action {
	Single(ActionKey, Option<OrderedFloat<f32>>),
	/// A "key" made up of multiple individual keys, for requiring multiple keys to be pressed to trigger an event.
	/// For example: `Alt+Enter`, or `Shift+F5`.
	Combo(Vec<ActionKey>, Option<OrderedFloat<f32>>),
}
impl Action {
	pub fn new_single(key: ActionKey, scale: Option<f32>) -> Self {
		Self::Single(key, scale.map(|v| OrderedFloat::from_f32(v).unwrap()))
	}
	pub fn new_combo(keys: Vec<ActionKey>, scale: Option<f32>) -> Self {
		Self::Combo(keys, scale.map(|v| OrderedFloat::from_f32(v).unwrap()))
	}
}

fn scancodes_to_actions<'a>(scancodes: impl Iterator<Item = &'a u32>) -> Vec<Action> {
	let mut ret = Vec::with_capacity(scancodes.size_hint().0);
	for &scancode in scancodes {
		ret.push(Action::Single(ActionKey::ScancodeId(scancode), None));
	}
	ret
}

/// # InputMap
/// This represents an input map that stores the state of keys, represented either by scancode or `VirtualKeyCode`.
///
/// ## Actions
/// Actions are a helper concept, making it easier to connect game actions to key events.
/// Setup just requires passing in the name of the action and the set of keys that should trigger that action.
/// Once set up, simply checking the action name will get whether any of the set keys is pressed.
/// This makes changing key layouts much easier - instead of finding every use of `KEY_W` in your code, you can just
/// change the declaration of the `"walk_forward"` action for example, or even redefine it at any time in your code!
///
/// **Warning:** This structure does not implicitly convert between VirtualKeyCodes and scancodes; the physical key and virtual key `Q`, for example,
/// are not guaranteed to be the same!
#[derive(Serialize, Deserialize, Debug)]
pub struct InputMap {
	// Value is (pressed, just_changed)
	#[serde(skip)]
	keys: HashMap<ActionKey, (bool, bool)>,
	#[serde(default)]
	actions: HashMap<String, Vec<Action>>,
	#[serde(skip)]
	mouse: MouseInput,
}

impl InputMap {
	/// Constructs a new, empty `InputMap`.
	pub fn new() -> Self {
		InputMap {
			keys: HashMap::new(),
			actions: HashMap::new(),
			mouse: MouseInput::new(),
		}
	}

	/// Helper function that checks if a scancode is pressed.
	///
	/// **Warning:** There is no guarantee for scancodes and virtual keys for the same key to be the same; it is up to you to make sure they stay in sync!
	pub fn is_scancode_pressed(&self, scancode: u32) -> bool {
		self.get_key_state(&ActionKey::ScancodeId(scancode))
			.0
	}

	fn get_key_state(&self, key: &ActionKey) -> &(bool, bool) {
   	self.keys.get(key).unwrap_or(&(false, false))
}

	/// Helper function that sets whether a scancode is pressed.
	///
	/// **Warning:** There is no inherent guarantee for scancodes and virtual keys for the same key to be the same; it is up to you to make sure they stay in sync!
	pub fn set_scancode_pressed(&mut self, scancode: u32, pressed: bool) {
		self
			.keys
			.insert(ActionKey::ScancodeId(scancode), (pressed, true));
	}

	fn get_keys_from_action<'a>(action: &'a Action) -> Box<dyn Iterator<Item = (&ActionKey, &Option<OrderedFloat<f32>>)> + 'a> {
		match action {
			Action::Combo(keylist, scale) => 
				Box::new(keylist.iter().zip(std::iter::once(scale))),
			Action::Single(k, scale) => 
				Box::new(std::iter::once(k).zip(std::iter::once(scale))),
	  }
	}

	/// Calls the given closure on all keys in a given action.
	/// If the closure returns true, the loop will exit early.
	/// ## Return
	/// Returns true if the closure returned true and exited early.
	fn for_actions_keys<F: FnMut(&ActionKey, &Option<OrderedFloat<f32>>) -> bool>(action: &Action, mut f: F) -> bool {
		let keys_to_check = Self::get_keys_from_action(action);
		for (key, scale) in keys_to_check {
			if f(key, scale) {
				return true;
			}
		}
		false
	}

	/// Checks whether a key is pressed.
	///
	/// **Warning:** There is no guarantee for scancodes and virtual keys for the same key to be the same; it is up to you to make sure they stay in sync!
	pub fn is_key_pressed(&self, action: &Action) -> bool {
		Self::for_actions_keys(action, |key, _| 
			self.get_key_state(key).0)
	}

	/// Checks whether a key was just pressed this frame.
	///
	/// **Warning:** There is no guarantee for scancodes and virtual keys for the same key to be the same; it is up to you to make sure they stay in sync!
	pub fn is_key_just_pressed(&self, action: &Action) -> bool {
		Self::for_actions_keys(action, |key, _| {
			let &(pressed, just_changed) = self.get_key_state(&key);
			pressed && just_changed
		})
	}
	/// Checks whether a key was just released this frame.
	///
	/// **Warning:** There is no guarantee for scancodes and virtual keys for the same key to be the same; it is up to you to make sure they stay in sync!
	pub fn is_key_just_released(&self, action: &Action) -> bool {
		Self::for_actions_keys(action, |key, _| {
			let &(pressed, just_changed) = self.get_key_state(&key);
			(!pressed) && just_changed
		})
	}

	pub fn get_key_strength(&self, action: &Action) -> f32 {
		let mut net = 0.0;
		Self::for_actions_keys(action, |key, scale| {
			if let Some(scale) = scale {
				if self.get_key_state(&key).0 {
					net = net + scale.0;
				}
			} 
			false
		});
		net
	}

	pub fn add_mouse_motion(&mut self, motion: Vector2) {
		self.mouse.add_motion(motion);
	}

	pub fn get_mouse_motion(&self) -> Vector2 {
		self.mouse.get_motion()
	}
	pub fn clear_mouse_motion(&mut self) {
		self.mouse.clear_motion();
	}

	/// Sets whether a key is pressed.
	///
	/// **Warning:** There is no guarantee for scancodes and virtual keys for the same key to be the same; it is up to you to make sure they stay in sync!
	pub fn set_virtual_key_pressed(&mut self, key: VirtualKeyCode, pressed: bool) {
		self.set_key_pressed(ActionKey::VirtualId(key), pressed);
	}
	/// Sets whether a key is pressed.
	///
	/// **Warning:** There is no guarantee for scancodes and virtual keys for the same key to be the same; it is up to you to make sure they stay in sync!
	pub fn set_scancode_key_pressed(&mut self, key: ScanCode, pressed: bool) {
		self.set_key_pressed(ActionKey::ScancodeId(key), pressed);
	}
	fn set_key_pressed(&mut self, key: ActionKey, pressed: bool) {
		if let Some((w_pressed, _)) = self.keys.get(&key) {
			if *w_pressed != pressed {
				self.keys.insert(key, (pressed, true));
			}
		} else {
			self.keys.insert(key, (pressed, true));
		}
	}

	pub fn update_keys(&mut self, input: KeyboardInput) {
		self.set_key_pressed(
			ActionKey::ScancodeId(input.scancode),
			input.state == ElementState::Pressed,
		);
		if let Some(keycode) = input.virtual_keycode {
			self.set_key_pressed(
				ActionKey::VirtualId(keycode),
				input.state == ElementState::Pressed,
			);
		}
	}

	/// Checks whether an action is pressed.
	///
	/// **Warning:** There is no guarantee for scancodes and virtual keys for the same key to be the same; it is up to you to make sure they stay in sync!
	pub fn is_action_pressed(&self, action: &str) -> bool {
		let opt_keys_for_action = self.actions.get(&String::from(action));
		if let Some(keys_for_action) = opt_keys_for_action {
			for key in keys_for_action.iter() {
				if self.is_key_pressed(key) {
					return true;
				}
			}
		}
		// if none of the keys were true, return false
		false
	}
	/// Checks whether an action was just pressed.
	///
	/// **Warning:** There is no guarantee for scancodes and virtual keys for the same key to be the same; it is up to you to make sure they stay in sync!
	pub fn is_action_just_pressed(&self, action: &str) -> bool {
		if let Some(keys_for_action) = self.actions.get(&String::from(action)) {
			for key in keys_for_action.iter() {
				if self.is_key_just_pressed(key) {
					return true;
				}
			}
		}
		// if none of the keys were true, return false
		false
	}
	/// Checks whether an action was just released.
	///
	/// **Warning:** There is no guarantee for scancodes and virtual keys for the same key to be the same; it is up to you to make sure they stay in sync!
	pub fn is_action_just_released(&self, action: &str) -> bool {
		let mut pressed = false;
		let mut just_released = false;
		if let Some(actions) = self.actions.get(&String::from(action)) {
			for action in actions.iter() {
				just_released = just_released || self.is_key_just_released(action);
				pressed = pressed || self.is_key_pressed(action);
			}
		}
		// only "just released" if all component keys are released
		// (it would be weird to stop running because you released the up arrow while still holding down W)
		just_released && !pressed
	}

	pub fn get_action_strength(&self, action: &str) -> f32 {
		let mut net = 0.0;
		if let Some(keys_for_action) = self.actions.get(&String::from(action)) {
			for key in keys_for_action.iter() {
				net += self.get_key_strength(key);
			}
		}
		net.clamp(-1.0, 1.0)
	}

	/// This is a helper function that implicitly converts `u32`s to `ScancodeId(u32)`s
	///
	/// This function will create a new action, or overwrite an old one with the same name.
	pub fn create_action_scancodes(&mut self, action: &str, keys: &Vec<u32>) {
		self
			.actions
			.insert(action.into(), scancodes_to_actions(keys.iter()));
	}
	/// This function will create a new action, overwriting any old one with the same name, with the given keys as inputs.
	pub fn create_action_vec(&mut self, action: &str, keys: Vec<Action>) {
		self.actions.insert(action.into(), keys);
	}
	
	/// This function will create a new action, overwriting any old one with the same name, with the given keys as inputs.
	pub fn with_action_vec(mut self, action: &str, keys: Vec<Action>) -> Self {
		self.actions.insert(action.into(), keys);
		self
	}
	
	/// This function will create a new action, overwriting any old one with the same name, with the given key as it's input.
	pub fn create_action(&mut self, action: &str, key: Action) {
		self.actions.insert(action.into(), vec![key]);
	}

	// Weak create action commands to only register keys for actions if the action does not exist

	/// This is a helper function that implicitly converts `u32`s to `ScancodeId(u32)`s
	///
	/// This function will create a new action, but will not override an old one of the same name.
	pub fn weak_create_action_scancodes(&mut self, action: &str, keys: &Vec<u32>) {
		if !self.actions.contains_key(action.into()) {
			self.create_action_scancodes(action, &keys);
		}
	}
	/// This function will create a new action, but will not override an old one of the same name.
	pub fn weak_create_action_vec(&mut self, action: &str, keys: Vec<Action>) {
		if !self.actions.contains_key(action.into()) {
			self.create_action_vec(action, keys);
		}
	}
	/// This function will create a new action, but will not override an old one of the same name.
	pub fn weak_with_action_vec(mut self, action: &str, keys: Vec<Action>) -> Self{
		if !self.actions.contains_key(action.into()) {
			self.create_action_vec(action, keys);
		}
		self
	}
	/// This function will create a new action, but will not override an old one of the same name.
	pub fn weak_create_action(&mut self, action: &str, key: Action) {
		if !self.actions.contains_key(action.into()) {
			self.create_action(action, key);
		}
	}

	pub fn add_key_to_action(&mut self, action: &str, key: Action) {
		if let Some(action) = self.actions.get_mut(action.into()) {
			action.push(key);
		} else {
			self.create_action(action, key);
		}
	}
	pub fn add_keys_to_action(&mut self, action: &str, mut keys: Vec<Action>) {
		if let Some(action) = self.actions.get_mut(action.into()) {
			action.append(&mut keys);
		} else {
			self.create_action_vec(action, keys);
		}
	}
	/// Clears the `just_changed` field for all keys; call this at the end of a frame.
	pub fn step_frame(&mut self) {
		self.mouse.clear_motion();
		for (_, (_, recently_changed)) in self.keys.iter_mut() {
			*recently_changed = false;
		}
	}
}

#[derive(Debug)]
pub struct MouseInput {
	accumulated_motion: Vector2,
}
impl MouseInput {
	pub fn new() -> Self {
		MouseInput {
			accumulated_motion: Vector2::new(0.0, 0.0),
		}
	}
	pub fn get_motion(&self) -> Vector2 {
		self.accumulated_motion
	}
	pub fn add_motion(&mut self, delta: Vector2) {
		self.accumulated_motion += delta;
	}
	// pub fn add_motion(&mut self, delta: (f32, f32)) {
	// 	self.accumulated_motion += delta;
	// }
	pub fn clear_motion(&mut self) {
		self.accumulated_motion = Vector2::new(0.0, 0.0);
	}
}

impl Default for MouseInput {
    fn default() -> Self {
        Self::new()
    }
	
}

#[macro_export]
macro_rules! __input_sub {
	($ed:ident) => {
		input_map::Action::new_single($crate::ActionKey::VirtualId(winit::event::VirtualKeyCode::$ed), None)
	};
	({$ed:ident}) => {
		input_map::__input_sub!($ed)
	};
	({$ed:ident,}) => {
		input_map::__input_sub!($ed)
	};
	({$ed:ident, $scale:expr}) => {
		input_map::Action::new_single($crate::ActionKey::VirtualId(winit::event::VirtualKeyCode::$ed), Some($scale))
	};
	({$ed:ident, $scale:expr},) => {
		input_map::__input_sub!({$ed, $scale})
	};
}

#[macro_export]
macro_rules! with_input {
	($iname:expr, $($id:ident : [$($ed: tt),*]),+) => {
		$iname
		$(.with_action_vec(stringify!($id), vec![
			$(input_map::__input_sub!($ed)),*
		]))+
	};
	($iname:expr, $($id:ident : [$($ed: tt),*]),+,) => {
		with_input!($iname, $($id : [$($ed),*]),+)
	};
}

#[macro_export]
macro_rules! add_input {
	($iname:expr, $($id:ident : [$($ed: tt),*]),+) => {
		$iname
		$(.with_weak_action_vec(stringify!($id), vec![
			$(__input_sub!($ed)),*
		]))+
	};
	($iname:expr, $($id:ident : [$($ed: tt),*]),+,) => {
		with_input!($iname, $($id : [$($ed),*]),+)
	};
}

// *if this works as I think it does* this removes the module unless the project is compiled in test mode
#[cfg(test)]
mod tests {
	use super::InputMap;

	#[test]
	pub fn test_keys_input_map() {
		let mut input_map = InputMap::new();
		input_map.set_scancode_pressed(2, true);
		input_map.set_scancode_pressed(6, true);
		input_map.set_scancode_pressed(3, false);
		assert_eq!(input_map.is_scancode_pressed(2), true);
		assert_eq!(input_map.is_scancode_pressed(6), true);
		assert_eq!(input_map.is_scancode_pressed(3), false);
		assert_eq!(input_map.is_scancode_pressed(5), false);
	}

	#[test]
	pub fn test_actions_input_map() {
		let mut input_map = InputMap::new();
		input_map.set_scancode_pressed(2, true);
		input_map.set_scancode_pressed(6, true);
		input_map.set_scancode_pressed(3, false);

		input_map.create_action_scancodes("potato", &vec![2]);
		input_map.create_action_scancodes("aliens", &vec![8]);
		input_map.create_action_scancodes("evil", &vec![6, 3]);
		input_map.create_action_scancodes("dark", &vec![3, 6]);
		input_map.create_action_scancodes("santa", &vec![3]);

		assert_eq!(input_map.is_action_pressed("potato"), true);
		assert_eq!(input_map.is_action_pressed("aliens"), false);
		assert_eq!(input_map.is_action_pressed("evil"), true);
		assert_eq!(input_map.is_action_pressed("dark"), true);
		assert_eq!(input_map.is_action_pressed("santa"), false);
	}
}
